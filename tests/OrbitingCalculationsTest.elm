module OrbitingCalculationsTest (..) where

import ElmTest exposing (..)
import OrbitingCalculations exposing (..)


calculateChangeInVelocity : Test
calculateChangeInVelocity =
  suite
    "calculate change position"
    [ test
        "with a decimal number"
        (assertEqual
          (calculateChangeInPostiion { x = 2, y = 3 } 0.123456789)
          { x = 0.246913578, y = 0.370370367 }
        )
    , test
        "with a integer"
        (assertEqual
          (calculateChangeInPostiion { x = 2, y = 3 } 5)
          { x = 10, y = 15 }
        )
    , test
        "with negative numbers"
        (assertEqual
          (calculateChangeInPostiion { x = -3, y = -1 } 10)
          { x = -30, y = -10 }
        )
    ]


finalVelocityTest : Test
finalVelocityTest =
  suite
    "final position"
    [ test
        "with positive values"
        (assertEqual
          (finalPosition { x = 2, y = 4 } { x = 2, y = 1 })
          { x = 4, y = 5 }
        )
    , test
        "with negative values"
        (assertEqual
          (finalPosition { x = -1, y = -3 } { x = -4, y = -3 })
          { x = -5, y = -6 }
        )
    , test
        "with mixed types of numbers"
        (assertEqual
          (finalPosition { x = 0, y = -4 } { x = 9, y = 4 })
          { x = 9, y = 0 }
        )
    ]


calculateChangeInPostiionTest : Test
calculateChangeInPostiionTest =
  suite
    "calculate change position"
    [ test
        "with a decimal number"
        (assertEqual
          (calculateChangeInPostiion { x = 2, y = 3 } 0.123456789)
          { x = 0.246913578, y = 0.370370367 }
        )
    , test
        "with a integer"
        (assertEqual
          (calculateChangeInPostiion { x = 2, y = 3 } 5)
          { x = 10, y = 15 }
        )
    , test
        "with negative numbers"
        (assertEqual
          (calculateChangeInPostiion { x = -3, y = -1 } 10)
          { x = -30, y = -10 }
        )
    ]


finalPositionTest : Test
finalPositionTest =
  suite
    "final position"
    [ test
        "with positive values"
        (assertEqual
          (finalPosition { x = 2, y = 4 } { x = 2, y = 1 })
          { x = 4, y = 5 }
        )
    , test
        "with negative values"
        (assertEqual
          (finalPosition { x = -1, y = -3 } { x = -4, y = -3 })
          { x = -5, y = -6 }
        )
    , test
        "with mixed types of numbers"
        (assertEqual
          (finalPosition { x = 0, y = -4 } { x = 9, y = 4 })
          { x = 9, y = 0 }
        )
    ]


run : Test
run =
  suite
    "Testing the Orbiting Calculations"
    [ calculateChangeInPostiionTest
    , finalPositionTest
    ]
