module Main where

import Signal exposing (Signal)

import ElmTest exposing (consoleRunner)
import Console exposing (IO, run)
import Task

import OrbitingCalculationsTest

console : IO ()
console = consoleRunner OrbitingCalculationsTest.run

port runner : Signal (Task.Task x ())
port runner = run console
