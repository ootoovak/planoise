Planoise
========

Install
-------
`elm package install`
`npm i -g elm-live`

Run
---
`elm-live src/System.elm --open`
