module OrbitingCalculations (..) where


type alias Scalar =
  { x : Float
  , y : Float
  }


type alias Position =
  Scalar


type alias Velocity =
  Scalar


type alias Acceleration =
  Scalar


xDistance : Position -> Position -> Float
xDistance position1 position2 =
  position1.x - position2.x


yDistance : Position -> Position -> Float
yDistance position1 position2 =
  position1.y - position2.y


calculateAngle : Position -> Position -> Float
calculateAngle position1 position2 =
  atan2 (xDistance position1 position2) (yDistance position1 position2)


calculateRadius : Position -> Position -> Float
calculateRadius position1 position2 =
  let
    xDistance2 =
      (xDistance position1 position2) ^ 2

    yDistance2 =
      (yDistance position1 position2) ^ 2
  in
    sqrt (xDistance2 + yDistance2)


gravitationalConstant : Float
gravitationalConstant =
  6.67259


gravitationalAcceleration : Float -> Float -> Float
gravitationalAcceleration mass radius =
  (gravitationalConstant * mass) / (radius ^ 2)


calculateNewAcceleration : Position -> Position -> Float -> Acceleration
calculateNewAcceleration position1 position2 mass =
  let
    angle =
      calculateAngle position1 position2

    radius =
      calculateRadius position1 position2

    newAcceleration =
      gravitationalAcceleration mass radius
  in
    { x = newAcceleration * (sin angle)
    , y = newAcceleration * (cos angle)
    }


addScalars : Scalar -> Scalar -> Scalar
addScalars scalar newScalar =
  { scalar
    | x = scalar.x + newScalar.x
    , y = scalar.y + newScalar.y
  }


multiplyScalarByNumber : Scalar -> Float -> Scalar
multiplyScalarByNumber scalar multiplier =
  { scalar
    | x = scalar.x * multiplier
    , y = scalar.y * multiplier
  }


calculateChangeInVelocity : Acceleration -> Float -> Acceleration
calculateChangeInVelocity acceleration time =
  multiplyScalarByNumber acceleration time


finalVelocity : Velocity -> Velocity -> Velocity
finalVelocity initialVelocity changeInVelocity =
  addScalars initialVelocity changeInVelocity


calculateChangeInPostiion : Velocity -> Float -> Velocity
calculateChangeInPostiion velocity time =
  multiplyScalarByNumber velocity time


finalPosition : Position -> Position -> Position
finalPosition initialPosition changeInPosition =
  addScalars initialPosition changeInPosition
