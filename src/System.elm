module System (..) where

import Color exposing (..)
import Graphics.Collage exposing (..)
import Graphics.Element exposing (..)
import Time exposing (..)
import OrbitingCalculations exposing (..)
import Debug exposing (..)


type alias Location =
  OrbitingCalculations.Position


type alias Velocity =
  OrbitingCalculations.Velocity


type alias Density =
  { volume : Float
  , mass : Float
  }


type alias SolarBody =
  { color : Color
  , density : Density
  , location : Location
  , velocity : Velocity
  }


type alias System =
  { star : SolarBody
  , planets : List SolarBody
  }


newDensity : Float -> Float -> Density
newDensity volume mass =
  { volume = volume
  , mass = mass
  }


newLocation : Float -> Float -> Location
newLocation x y =
  { x = x
  , y = y
  }


newVelocity : Float -> Float -> Velocity
newVelocity x y =
  { x = x
  , y = y
  }


newSolarBody : Color -> Density -> Location -> Velocity -> SolarBody
newSolarBody color density location velocity =
  { color = color
  , density = density
  , location = location
  , velocity = velocity
  }



----------


calculateSolarBodyVelocity : SolarBody -> SolarBody -> SolarBody
calculateSolarBodyVelocity star solarBody =
  let
    position1 =
      star.location

    position2 =
      solarBody.location

    mass =
      star.density.mass

    acceleration =
      (calculateNewAcceleration position1 position2 mass)

    calculatedVelocity =
      (calculateChangeInVelocity acceleration 100)
  in
    { solarBody | velocity = (newVelocity calculatedVelocity.x calculatedVelocity.y) }


calculateSolarBodyLocation : SolarBody -> SolarBody
calculateSolarBodyLocation solarBody =
  let
    newX =
      solarBody.location.x + solarBody.velocity.x

    newY =
      solarBody.location.y + solarBody.velocity.y
  in
    { solarBody | location = (log "location" (newLocation newX newY)) }


calculateNextSolarBodySate : SolarBody -> SolarBody -> SolarBody
calculateNextSolarBodySate star solarBody =
  solarBody
    |> calculateSolarBodyVelocity star
    |> calculateSolarBodyLocation



----------


renderSolarBody : SolarBody -> Form
renderSolarBody solarBody =
  circle solarBody.density.volume
    |> filled solarBody.color
    |> move ( solarBody.location.x, solarBody.location.y )


renderAllSolarBodys : System -> List Form
renderAllSolarBodys system =
  let
    solarBodies =
      system.star :: system.planets
  in
    List.map renderSolarBody solarBodies



----------


initialSystem : System
initialSystem =
  { star = newSolarBody yellow (newDensity 50 100) (newLocation 0 0) (newVelocity 0 0)
  , planets =
      [ newSolarBody orange (newDensity 25 55) (newLocation 200 200) (newVelocity 0 200)
        --, newSolarBody blue (newDensity 25 45) (newLocation 0 300) (newVelocity 0 300)
        --, newSolarBody red (newDensity 15 25) (newLocation  0 400) (newVelocity 0 400)
      ]
  }


update : Time.Time -> System -> System
update time system =
  let
    calculateNextStep solarBody =
      calculateNextSolarBodySate system.star solarBody
  in
    { system | planets = List.map calculateNextStep system.planets }


view : ( Int, Int ) -> System -> Element
view ( width, height ) system =
  (log "--- system ---" system)
    |> renderAllSolarBodys
    |> collage width height
