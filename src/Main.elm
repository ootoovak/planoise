module Main (..) where

import Graphics.Element exposing (Element)
import Time exposing (fps, inSeconds)
import Window exposing (dimensions)
import System exposing (..)


framesPerSecond =
  100


timer : Signal Time.Time
timer =
  Signal.map inSeconds (fps framesPerSecond)


gameState : Signal System
gameState =
  Signal.foldp update initialSystem timer


main : Signal Element
main =
  Signal.map2 view Window.dimensions gameState
